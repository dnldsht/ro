
# Il problema dei compagni di merende. 
## es 1.4.1
#### Soluzione GNU Linear Programming Kit
```bash
$ glpsol -m zaino.mod -d zaino.dat -y zaino.sol -o zaino.sol
$ vim zaino.sol
```
#### Problema
Un gruppo di amici dovendo fare
una gita ha deciso di mettere cibi e bevande in un unico zaino da 10 Kg. Lo zaino può
essere riempito con

1. Cioccolata (confezioni da 500 g.)
2. Succhi di frutta (bottiglie da 1 l.)
3. Lattine di birra (formato da 0.33 l.)
4. Panini imbottiti (da 100 g. l’uno)
5. Acqua minerale (bottiglie da 1 l.)
6. Pacchi di biscotti (confezioni da 500 g.)

Dopo un’indagine tra i partecipanti alla gita (si poteva dare un voto da 1 a 100 ad ogni
prodotto) sono stati determinati i seguenti punteggi.

| Prodotto          | Punti |
|-------------------|------:|
| Cioccolata        | 10    |
| Succhi di frutta  | 30    |
| Lattine di birra  | 6     |
| Panini imbottiti  | 20    |
| Acqua minerale    | 20    |
| Pacchi di biscotti| 8     |


Per non scontentare nessuno si è deciso di portare almeno:
- 2 confezioni di cioccolata;
- 2 bottiglie di succo di frutta;
- 6 lattine di birra;
- 10 panini imbottiti;
- 2 conf. di biscotti.
Formulare il modello di Programmazione Lineare che massimizzi il punteggio rispettando
il vincolo di capacità dello zaino.

#### Focus
>Considerate le richieste minime di oggetti da inserire nello zaino, si potreb-
be essere portati a ridurre la capacità totale dello zaino dello spazio occupato per
soddisfare le richieste minime. Questa scelta – valida per il problema specifico in
esame – ha l’effetto di rendere il modello meno generale e più legato alla particolare
istanza.
In tal caso, quali sono le modifiche da apportare al modello proposto come solu-
zione? La soluzione del modello modificato come deve essere trattata rispetto al
problema dato?
