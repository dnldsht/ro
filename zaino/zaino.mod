/* Parametri */
set Prodotti;
param peso{t in Prodotti};
param punteggio{t in Prodotti};
param richiesta{t in Prodotti};
param capacita_zaino;


/* Variabili Decisionali */
var take{t in Prodotti}, integer, >=richiesta[t];

/* Funzione Obiettivo */
maximize punteggio_totale: sum{t in Prodotti} take[t] * punteggio[t];

/* Vincoli */
subject to peso_zaino: 
    sum{t in Prodotti} take[t] * peso[t] <= capacita_zaino;
 
solve;
printf '#################################\n\n';
printf "Punteggio: %i\n", punteggio_totale;
printf "Peso zaino: %.2f\n", peso_zaino;
printf "\n";
printf "Lo zaino contiene:\n";
printf {i in Prodotti} "%s: %i\n", i, take[i];
printf "\n";
printf '#################################\n\n';
end;
