Problem:    base
Rows:       7
Columns:    6 (6 integer, 0 binary)
Non-zeros:  18
Status:     INTEGER OPTIMAL
Objective:  assunzioni = 27 (MINimum)

   No.   Row name        Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 assunzioni                 27                             
     2 fa1                         4             4               
     3 fa2                         8             8               
     4 fa3                        10            10               
     5 fa4                         7             7               
     6 fa5                        13            12               
     7 fa6                        12            12               

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 x[1]         *              4             0               
     2 x[2]         *              4             0               
     3 x[3]         *              6             0               
     4 x[4]         *              1             0               
     5 x[5]         *             12             0               
     6 x[6]         *              0             0               

Integer feasibility conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
