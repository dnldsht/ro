set I;
param b{I};

var x{i in I}, integer, >= 0;

minimize assunzioni:
    sum {i in I} x[i];

fa1: x[1] + x[6] >= b[1];
fa2: x[1] + x[2] >= b[2];
fa3: x[2] + x[3] >= b[3];
fa4: x[3] + x[4] >= b[4];
fa5: x[4] + x[5] >= b[5]; 
fa6: x[5] + x[6] >= b[5];

solve; 

