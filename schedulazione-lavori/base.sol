Problem:    base
Rows:       15
Columns:    10 (10 integer, 0 binary)
Non-zeros:  33
Status:     INTEGER OPTIMAL
Objective:  errori = 6 (MINimum)

   No.   Row name        Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 errori                      6                             
     2 oi1                        -5                          -5 
     3 oi2                        -7                          -7 
     4 oi3                        -7                          -4 
     5 oi4                        -7                          -7 
     6 errore1                  -115          -117               
     7 errore3                   117           117               
     8 errore4                  -121          -121               
     9 errore5                   121           121               
    10 errore6                  -128          -128               
    11 errore7                   128           128               
    12 errore8                  -135          -135               
    13 errore9                   135           135               
    14 errore10                 -137          -137               
    15 errore11                  147           137               

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 t[1]         *            116             0               
     2 t[2]         *            121             0               
     3 t[3]         *            128             0               
     4 t[4]         *            135             0               
     5 t[5]         *            142             0               
     6 e[1]         *              1                             
     7 e[2]         *              0                             
     8 e[3]         *              0                             
     9 e[4]         *              0                             
    10 e[5]         *              5                             

Integer feasibility conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
