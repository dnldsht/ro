#################################

Profitto: 42300
PezziA: 4000
PezziB: 5676
UnitaLavoro: 699

Modello 1: 430 pezzi
Modello 2: 200 pezzi
Modello 3: 508 pezzi

#################################

ower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 profit                  42300                             
     2 risorse[PezziA]
                                4000                        4000 
     3 risorse[PezziB]
                                5676                        6000 
     4 risorse[UnitaLavoro]
                             699.316                         700 

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 x[1]         *            430           200               
     2 x[2]         *            200           200               
     3 x[3]         *            508           150               

Integer feasibility conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
