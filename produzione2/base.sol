#################################

Produzione ottimale:
Profitto: 42300
Pezzi A usati: 4000
Pezzi B usati: 5676
Forza Lavoro: 699

Modello 1: 430 pezzi
Modello 2: 200 pezzi
Modello 3: 508 pezzi

#################################

-------    ------------- ------------- -------------
     1 sumA                     4000                        4000 
     2 sumB                     5676                        6000 
     3 sumL                  699.164                         700 
     4 max_profitto            42300                             

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 take[1]      *            430           200               
     2 take[2]      *            200           200               
     3 take[3]      *            508           150               

Integer feasibility conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
