set I;
set J;

param p{i in I};
param r{i in I};

param c{j in J, i in I};

param total{j in J};


var x{i in I}, integer, >= r[i];

maximize profit: sum{i in I} x[i] * p[i];

subject to risorse {j in J}: 
    sum {i in I} c[j, i] * x[i] <= total[j];

solve;

# Report
printf '#################################\n\n';
printf "Profitto: %i\n", profit;


for {j in J} {
    printf "%s: %i\n", j, risorse[j];
}

printf "\n";
printf {i in I} "Modello %i: %i pezzi\n", i, x[i];
printf "\n";
printf '#################################\n\n';
end;