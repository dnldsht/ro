# Il problema del distributore di giornali. 
## es 1.4.4
#### Soluzione GNU Linear Programming Kit
parte a
```bash
$ glpsol -d base.dat -m base.mod -o base.sol -y base.report
$ vim base.sol
```
parte b
```bash
$ glpsol -d base.dat -m parteb.mod -o parteb.sol -y parteb.report
$ vim parteb.sol
```
#### Problema
La casa editrice ANALFABETA
pubblica un quotidiano che viene distribuito da quattro centri di smistamento S1 , S2 ,
S3 , S4 che richiedono rispettivamente almeno 100000, 150000, 50000 e 75000 copie. Il
giornale viene stampato in tre tipografie T1 , T2, T3 che producono rispettivamente al
massimo 125000, 180000 e 70000 copie
I costi per la spedizione sono di 2 euro/Km. per giornale e le distanze tra le tipografie ed i
centri di smistamento sono rispettivamente di 20, 25, 15 e 5 Km. per la prima tipografia,
di 12, 14, 18 e 30 Km per la seconda, e di 19, 11, 40 e 12 Km per la terza.
(a) Formulare il modello di Programmazione Lineare per pianificare le spedizioni a costo
totale minimo.
(b) Si definisca il costo di approvvigionamento di un centro di smistamento come il costo
totale delle spedizioni verso quel centro. Formulare il modello di Programmazione
Lineare che minimizza il massimo costo di approvvigionamento.
