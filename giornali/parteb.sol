Problem:    parteb
Rows:       12
Columns:    13 (13 integer, 0 binary)
Non-zeros:  41
Status:     INTEGER OPTIMAL
Objective:  approvigionamenti = 1890000 (MINimum)

   No.   Row name        Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 approvigionamenti
                            1.89e+06                             
     2 richiesta[1]           100000        100000               
     3 richiesta[2]           150000        150000               
     4 richiesta[3]            50000         50000               
     5 richiesta[4]            75000         75000               
     6 risorse[1]             125000                      125000 
     7 risorse[2]             180000                      180000 
     8 risorse[3]              70000                       70000 
     9 approvigionamento[1]
                                   0                          -0 
    10 approvigionamento[2]
                                   0                          -0 
    11 approvigionamento[3]
                        -1.06305e+06                          -0 
    12 approvigionamento[4]
                                   0                          -0 

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 x[1,1]       *          86250             0               
     2 x[2,1]       *          13750             0               
     3 x[3,1]       *              0             0               
     4 x[1,2]       *              0             0               
     5 x[2,2]       *          80000             0               
     6 x[3,2]       *          70000             0               
     7 x[1,3]       *          24350             0               
     8 x[2,3]       *          25650             0               
     9 x[3,3]       *              0             0               
    10 x[1,4]       *          14400             0               
    11 x[2,4]       *          60600             0               
    12 x[3,4]       *              0             0               
    13 y            *       1.89e+06             0               

Integer feasibility conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
