set I;
set J;

param p{i in I};
param r{j in J};

param d{i in I, j in J};

param costoKm;

var x{i in I, j in J}, integer, >= 0;
var y, integer, >= 0;


minimize approvigionamenti: y;

subject to richiesta {j in J}: 
    sum {i in I} x[i, j] >= r[j];

s.t. risorse {i in I}: 
    sum{j in J} x[i,j] <= p[i];

s.t. approvigionamento {j in J}: 
    sum {i in I} d[i, j] * x[i, j] <= y;

solve;

# Report
printf '#################################\n\n';


printf 'Spedizioni\n';
printf{i in I, j in J}:' T%i -> S%i:  %i (%.2f$)\n',i,j, x[i, j], (x[i, j] * d[i, j]) * costoKm;
printf '\n';
printf "Produzione massima\n";
for {i in I} {
    printf ' T%i %i <= %i\n', i, sum {j in J} x[i,j], p[i];
}

printf '\n';
printf "Richiesta minima\n";
for {j in J} {
    printf ' S%i %i >= %i\n', j, sum {i in I} x[i,j], r[j];
}

printf '\n';

printf "Spesa per centro\n";
for {j in J} {
    printf " S%i (%.2f$)\n", j, sum {i in I} (x[i, j] * d[i, j]) * costoKm;
    
}

printf "Costo totale: %.2f$\n", (sum {j in J} (sum {i in I} (x[i, j] * d[i, j]))) * costoKm;

printf "\n";
printf '#################################\n\n';
end;